//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
    short a;
    __asm{
        mov ax, 18
        add ax, 8
        mov a, ax
    };
    Edit1->Text = a;

   int b =  sizeof(short);
   int c = b;

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
    short a;
    __asm{
        mov ax, 0x4E20
        add ax, 0x1406
        mov bx, 0x2000
        add ax, bx
        mov bx, ax
        add ax, bx
        mov a, ax
    };
    Edit1->Text = a;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
    char a;
    __asm{
        mov ax, 0x001A
        mov bx, 0x0026
        add al, bl
        add ah, bl
        add bh, al
        mov ah, 0
        add al, 0x85
        add al, 0x93
        mov a, al
    };
    Edit1->Text = a;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
    short a;
    __asm{
        mov ax, 2
        add ax, ax
        add ax, ax
        add ax, ax
        mov a, ax
    };
    Edit1->Text = a;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
    char a;
    __asm{
        mov bx, 0x0000
        mov ds, bx
        mov [0], al
        mov a, al
    };
    Edit1->Text = a;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender)
{
    char a;
    __asm{
        mov ax, 0x1000
        mov ds, ax
        mov ax, 11316
        mov [0], ax
        mov bx, [0]
        sub bx, [2]
        mov [2], bx

    };
    Edit1->Text = a;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button7Click(TObject *Sender)
{
    short a;
    __asm{
        mov ax,0x0123
        push ax
        mov bx,0x2266
        push bx
        mov cx,0x1122
        push cx
        pop ax
        pop bx
        pop cx
        mov a, cx

    };
    Edit1->Text = a;
}
//---------------------------------------------------------------------------

